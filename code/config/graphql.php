<?php

return [

    // The prefix for routes
    'prefix' => 'graphql',


    'routes' => '{graphql_schema?}',


    'controllers' => \Folklore\GraphQL\GraphQLController::class.'@query',


    'variables_input_name' => 'params',


    'middleware' => [],

    'graphiql' => [
        'routes' => '/graphiql',
        'middleware' => [],
        'view' => 'graphql::graphiql'
    ],


    'schema' => 'default',

    'schemas' => [
        'default' => [
            'query' => [
                'allVideos' => \App\GraphQL\Query\AllVideosQuery::class,
                'videoById' => \App\GraphQL\Query\VideoByIdQuery::class,
                'userById' => \App\GraphQL\Query\UserByIdQuery::class,
                'followers' => \App\GraphQL\Query\FollowersQuery::class,
                'users' => \App\GraphQL\Query\AllUsersQuery::class,


            ],
            'mutation' => [
                'signUp' => \App\GraphQL\Mutation\SignUpMutation::class,
                'logIn' => \App\GraphQL\Mutation\LogInMutation::class,
                'replayVideo' => \App\GraphQL\Mutation\ReplayToVideoMutation::class,
                'likeVideo' => \App\GraphQL\Mutation\LikeVideoMutation::class,
                'unlikeVideo' => \App\GraphQL\Mutation\UnlikeVideoMutation::class,
                'follow' => \App\GraphQL\Mutation\FollowMutation::class,
                'unfollow' => \App\GraphQL\Mutation\UnfollowMutation::class,


            ]
        ],
        'secret' => [
            'query' => [

            ],

        ]
    ],

    'types' => [
        'User' => \App\GraphQL\Type\UserType::class,
        'Video' => \App\GraphQL\Type\VideoType::class,
        'Replay' => \App\GraphQL\Type\ReplayType::class,
    ],

    // This callable will received every Error objects for each errors GraphQL catch.
    // The method should return an array representing the error.
    //
    // Typically:
    // [
    //     'message' => '',
    //     'locations' => []
    // ]
    //
    'error_formatter' => [\Folklore\GraphQL\GraphQL::class, 'formatError'],

    // Options to limit the query complexity and depth. See the doc
    // @ https://github.com/webonyx/graphql-php#security
    // for details. Disabled by default.
    'security' => [
        'query_max_complexity' => null,
        'query_max_depth' => null
    ]
];

