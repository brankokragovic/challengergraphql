<?php

namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class ReplayType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Replay',
        'description' => 'Replay to video'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of a replay'
            ],
            'user' => [
                'type' => Type::nonNull(GraphQL::type('User')),
                'description' => 'The user that posted a replay'
            ],
            'video' => [
                'type' => Type::nonNull(GraphQL::type('Video')),
                'description' => 'The video that was replied to'
            ],
            'replay' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'The replay'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Date a video was created'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date a video was updated'
            ],
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at;
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at;
    }
}