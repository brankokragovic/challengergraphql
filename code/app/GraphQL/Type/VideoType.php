<?php

namespace App\GraphQL\Type;

use GraphQL;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as GraphQLType;

class VideoType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Video',
        'description' => 'User video'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'The id of a video'
            ],

            'user' => [
                'type' => Type::nonNull(GraphQL::type('User')),
                'description' => 'The user that posted a video'
            ],
            'video' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Video'
            ],
            'created_at' => [
                'type' => Type::string(),
                'description' => 'Date a video was created'
            ],
            'updated_at' => [
                'type' => Type::string(),
                'description' => 'Date a video was updated'
            ],
            'replies' => [
                'type' => Type::listOf(GraphQL::type('Replay')),
                'description' => 'The replies to a video'
            ],
            'likes_count' => [
                'type' => Type::int(),
                'description' => 'The number of likes on a video'
            ],
        ];
    }

    protected function resolveCreatedAtField($root, $args)
    {
        return (string) $root->created_at;
    }

    protected function resolveUpdatedAtField($root, $args)
    {
        return (string) $root->updated_at;
    }

    protected function resolveLikesCountField($root, $args)
    {
        return $root->likes->count();
    }
}