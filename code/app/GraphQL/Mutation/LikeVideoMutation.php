<?php

namespace App\GraphQL\Mutation;

use App\Like;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\Video;

class LikeVideoMutation extends Mutation
{
    protected $attributes = [
        'name' => 'likeVideo'
    ];

    public function type()
    {
        return Type::string();
    }

    public function args()
    {
        return [
            'video_id' => [
                'name' => 'video_id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
        ];
    }

    public function authenticated($root, $args, $currentUser)
    {
        return !!$currentUser;
    }

    public function resolve($root, $args)
    {
        $video = Video::find($args['video_id']);

        $like = new Like();
        $like->user_id = auth()->user()->id;
        $video->likes()->save($like);

        return 'Like successful!';
    }
}