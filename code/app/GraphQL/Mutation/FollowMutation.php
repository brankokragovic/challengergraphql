<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\Follow;
use GraphQL;
use App\User;
class FollowMutation extends Mutation
{
    protected $attributes = [
        'name' => 'followUser'
    ];

    public function type()
    {
        return Type::string();
    }

    public function args()
    {
        return [
            'user_id' => [
                'name' => 'user_id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
        ];
    }
    public function authenticated($root, $args, $currentUser)
    {

        return !!$currentUser;
    }

    public function resolve($root, $args)
    {
        if(auth()->user()->id == $args['user_id'])
        {
            return 'You can\'t follow yourself';
        }
        if (!auth()->user()->isFollowing($args['user_id'])  ) {

            auth()->user()->follows()->create([
                'user_id' => auth()->user()->id,
                'target_id' => $args['user_id'],
            ]);

            return 'Well done!';
        } else {
            return 'You are already following this person';
        }
    }
}