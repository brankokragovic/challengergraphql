<?php

namespace App\GraphQL\Mutation;

use GraphQL;
use App\Video;
use App\Replay;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use Illuminate\Support\Facades\Auth;

class ReplayToVideoMutation extends Mutation
{
    protected $attributes = [
        'name' => 'replayVideo'
    ];

    public function type()
    {
        return GraphQL::type('Replay');
    }

    public function args()
    {
        return [
            'video_id' => [
                'name' => 'video_id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
            'replay' => [
                'name' => 'replay',
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required'],
            ]
        ];
    }
    public function authenticated($root, $args, $currentUser)
    {
        return !!$currentUser;
    }

    public function resolve($root, $args)
    {
        $video = Video::find($args['video_id']);

        $replay = new Replay();

        $replay->user_id = auth()->user()->id;

        $replay->replay = $args['replay'];

        $video->replies()->save($replay);

        return $replay;
    }
}