<?php

namespace App\GraphQL\Mutation;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;
use App\Follow;
use GraphQL;
use App\User;
class UnfollowMutation extends Mutation
{
    protected $attributes = [
        'name' => 'unfollowUser'
    ];

    public function type()
    {
        return Type::string();
    }

    public function args()
    {
        return [
            'user_id' => [
                'name' => 'user_id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
        ];
    }
    public function authenticated($root, $args, $currentUser)
    {

        return !!$currentUser;
    }

    public function resolve($root, $args)
    {
        if (auth()->user()->isFollowing($args['user_id']))
        {
            $follow = auth()->user()->follows()->where('target_id', $args['user_id'])->first();

            $follow->delete();

            return 'You are no longer follow this user';
        }
        else
        {
            return 'You are not following this person';
        }
    }
}