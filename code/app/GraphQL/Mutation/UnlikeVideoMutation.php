<?php

namespace App\GraphQL\Mutation;

use App\Like;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Mutation;

class UnlikeVideoMutation extends Mutation
{
    protected $attributes = [
        'name' => 'unlikeVideo'
    ];

    public function type()
    {
        return Type::string();
    }

    public function args()
    {
        return [
            'video_id' => [
                'name' => 'video_id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required'],
            ],
        ];
    }

    public function authenticated($root, $args, $currentUser)
    {
        return !!$currentUser;
    }

    public function resolve($root, $args)
    {
        $like = Like::where('user_id', auth()->user()->id)
            ->where('video_id', $args['video_id'])
            ->delete();

        return 'Unlike successful!';
    }
}