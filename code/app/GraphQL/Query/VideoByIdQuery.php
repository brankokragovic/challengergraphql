<?php

namespace App\GraphQL\Query;

use GraphQL;
use App\Video;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;

class VideoByIdQuery extends Query
{
    protected $attributes = [
        'name' => 'videoById'
    ];

    public function type()
    {
        return GraphQL::type('Video');
    }

    public function args()
    {
        return [
            'id' => [
                'name' => 'id',
                'type' => Type::nonNull(Type::int()),
                'rules' => ['required']
            ],
        ];
    }

    public function resolve($root, $args)
    {
        if (!$video = Video::find($args['id'])) {
            throw new \Exception('Resource not found');
        }

        return $video;
    }
}