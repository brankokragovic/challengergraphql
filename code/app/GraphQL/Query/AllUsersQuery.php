<?php

namespace App\GraphQL\Query;

use App\User;
use GraphQL;
use App\Video;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;

class AllUsersQuery extends Query
{
    protected $attributes = [
        'name' => 'allUsers'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('User'));
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $users = User::all();

        return $users;
    }
}