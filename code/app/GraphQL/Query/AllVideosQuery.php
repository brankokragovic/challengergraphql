<?php

namespace App\GraphQL\Query;

use GraphQL;
use App\Video;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;

class AllVideosQuery extends Query
{
    protected $attributes = [
        'name' => 'allVideos'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('Video'));
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $fields = $info->getFieldSelection();

        $videos = Video::query();

        foreach ($fields as $field => $keys) {
            if ($field === 'user') {
                $videos->with('user');
            }

            if ($field === 'replies') {
                $videos->with('replies');
            }

            if ($field === 'likes_count') {
                $videos->with('likes');
            }
        }

        return $videos->latest()->get();
    }
}