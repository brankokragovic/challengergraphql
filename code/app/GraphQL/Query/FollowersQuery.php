<?php

namespace App\GraphQL\Query;

use App\Follow;
use App\User;
use GraphQL;
use App\Video;
use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;

class FollowersQuery extends Query
{
    protected $attributes = [
        'name' => 'followers'
    ];

    public function type()
    {
        return Type::listOf(GraphQL::type('User'));
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $users = auth()->user()->follows()->where('user_id', auth()->user()->id)->pluck('target_id');

        $followers = User::whereIn('id',$users)->get();

        return $followers;
    }
}