<?php

namespace App\Http\Controllers;

use App\User;
use App\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
class UploadController extends Controller
{
    public function uploadVideo(Request $request)
    {
        $data = $request->all();
        $rules = [
            'video' => 'mimes:mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts|max:100040|required'];
        $validator = Validator($data, $rules);

        if ($validator->fails())
        {
            return 'Unsupported format';
        } else {
            $file = $data['video'];
            $extension = pathinfo($file->getClientOriginalExtension(), PATHINFO_FILENAME);
            $input = hash('md5', $file->getClientOriginalName());
            $destinationPath = public_path('uploads/videos');
            $file->move($destinationPath, $input . '.' . $extension);

            $video = new Video();
            $video->user_id = auth()->user()->id;
            $video->video = $input . '.' . $extension;

            $video->save();

            $responce = 'Video have been created successfully';
            return response($responce, 200);
        }
    }

    public function uploadProfilePicture(Request $request)
    {
        $data = $request->all();
        $rules = [
            'profile_picture' => 'image|mimes:jpeg,jpg,png,bmp,gif,svg'];
        $validator = Validator($data, $rules);

        if ($validator->fails()) {

            $response = 'Unsupported format';
            return response($response,200);

        } else {

            $file = $data['profile_picture'];

            $extension = pathinfo($file->getClientOriginalExtension(), PATHINFO_FILENAME);
            $input = hash('md5', $file->getClientOriginalName());
            $destinationPath = public_path('uploads/pictures');
            $file->move($destinationPath, $input. '.' . $extension);

            $id = \auth()->user()->id;
            User::where('id',$id)->first()->update(array('profile_picture' => $input . '.' . $extension));
           // dd($user);


            $response = 'Profile picture have been upload successfully';
            return response($response,200);
        }
    }
}
